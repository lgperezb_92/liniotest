# LinioCodeTest

_Challenge enviado de parte del equipo de Linio. 
Se consume un servicio web (JSON) con un arreglo de colecciones de favoritos de un usuario._

### Pre-requisitos 📋
* Git-LFS
* CocoaPods

### Librerías 

* **IGListKit**: Aplicaciones del estilo de ventas, que ofrecen distitnas secciones customizables mediante el backend se ven beneficiadas con esta librería, al poder hacer cambios desde backend sin necesidad de realizar cambios a la UI en el front.
Como puede apreciarse, solo se recibe un array de secciones y los controllers se encargan de actualizar la UI, permitiendo incluso secciones horizontales y verticales así como combinación de celdas dentro de cada sección
* **ObjectMapper**: Cuenta con una amplia capacidad para hacer mapping de objetos, muy util para objetos complejos, en este caso se me hizo una buena demostración para reflejar la facilidad con que puede usarse, el manego de Genericos, Enums, Diccionarios también es mucho más sencillo en comparación con el protocolo Codable.

### Arquitectura 

La aplicación ha sido diseñada usando VIPER como modelo Base.

* FlowModule: Crea el presenter, interactor, router y carga la vista.
* Interactor: Comunica con el Network layer para descargar los favoritos.
* Presenter: Se comunica con el View, Interactor y Router
* View: Controller desde donde se crea la vista y se atienden eventos.

Adicional a VIPER, se tiene un híbrido copiando la idea a MVVM acerca del ViewModel.
En lugar de hacer complejo el Modelo y pasar un objeto completo a las celdas, el ViewModel se encarga de definir la UI para ser adaptado a una vista.

### UI/UX

* **Assets**: Se configuró Git LFS para los archivos png, en este proyecto de ejemplo no es tan necesario pero para aplicaciónes grandes es muy util para evitar proyectos muy pesados.
* **Cells**: Se utilizaron XIB's para crear las celdas, el uso de StackViews ayuda a poder ocultar facilmente elementos que puedan no estar presentes en el objeto, además la reorganización de elementos se vuelve mas sencilla al solo arrastrar y cambiarlos de posición, evitando así problemas con Autolayout cuando cada elemento se fijó usando contraints.
* **FavoritesVC**: El collectionView se agregó usando LayoutConstraints desde código para mostrar la alternativa, el VC es un XIB y se ejemplifica como inyectar dependencias en el inicializador de un VC.
* **Storyboard**: Se dejó el storyboard inicial por falta de tiempo y para ejemplificar su uso, en caso de desear eliminarlo solo se debe remover y cargar el UIWindow desde código usando el AppDelegate en la funcion _didFinishLaunchinWithOptions_

### API

Se usaron algunos principios de SOLID para definir el comportamiento de la librería de Network y posteriormente ser adaptada por una clase.
En este casó se eligió la librería nativa para networking.
El protocolo permite cambiar con mucha facilidad una librería sin afectar el resto del código.

### Error Handling

El manejo de errores solo se ejemplifica a nivel API, donde un customError fue creado y se regresa haciendo uso de <Result>.
Idealmente este error debería seguir llevandose hasta el presenter, para de esta manera mostrar algo al usuario.

### Documentación:

El código ha sido documentado siguiendo los estandares que Swift tiene, para definir parametros, returns, etc...
Esto permite exportar la documentación del proyecto en un formato adecuado y facilmente, además permite a los desarrolladores conocer más de una funcion con la opción "help".

### Nice to Have:

Idealmente con mayor tiempo y para un proyecto real, hay varias ideas y caminos que me gusta adoptar como lo que detallaré a continuación.

* **Swinject**: Un inyector de dependencias resulta muy util en proyecto grandes, además de que permite combinar A/B test para inyectar distintas dependencias a una clase y de esta manera evaluar cual se comporta mejor, sin mencionar todo el código ahorrado para crear init's y pasar entre clases las dependencias.
Como bonus, es bastante util para inyectar Mockups a los Tests
* **Unit Tests**: Idealmente el CodeCoverage de todo archivo relacionado a lógica, debería llevar al menos un 70% de coverage.
* **Promise Kit**: Esta librería es bastante util para el manejo de closures complejos, ademas de que facilita la lectura y manejo de errores, dependiendo del proyecto podría ser una buena herramienta.
* **Error Handling**: Como se mencionó anteriormente, el error debería de llegar hasta el presenter y tener UI dedicada a mostrar lo que pasa al usuario.
* **Lottie**: Como usuario me gusta ver una animación mientras algo carga y las nativas de iOS no son muy buenas, Lottie permite mostrar animaciones mas complejas que pueden dar impresión al usuario de una mayor seriedad de parte de la empresa.
* **UITests**: Es buena idea escribir tests de UI para hacer integración de todo y probar como funciona en conjunto, dado que los UnitTests solo prueban elementos aislados.
