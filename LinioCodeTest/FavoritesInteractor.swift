//
//  FavoritesInteractor.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import Foundation
import IGListKit

typealias InteractorCallback = (([FavoritesCollection]) -> Void)

class FavoritesInteractor {
    
    private var provider: NetworkLayer
    
    init(provider: NetworkLayer) {
        self.provider = provider
    }
    
    /**
        Downloads from a Get Service an Array of Favorites Collections.
        - Parameter completion: Answer with an Array of Sections/Collections for favorites. An empty array in case of error.
     */
    func getFavorites(completion: @escaping InteractorCallback) {
        provider
            .get(from: Constants.favoritesURL) { (result: Result<[FavoritesCollection], NetworkError>) in
                switch result {
                case .success(let sections):
                    completion(sections)
                case .failure(_):
                    completion([])
                }
        }
    }
    
}
