//
//  FavoritesFlowModule.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

class FavoritesFlowModule {
    
    private var presenter: FavoritesPresenter
    
    init(baseController: UIViewController) {
        let provider: NetworkLayer = NativeNetwork()
        let interactor = FavoritesInteractor(provider: provider)
        let router = FavoritesWireFrame(baseController: baseController)
        presenter = FavoritesPresenter(interactor: interactor, router: router)
    }
    
    // Starts flow for FavoritesVC
    func start() {
        presenter.start()
    }
}
