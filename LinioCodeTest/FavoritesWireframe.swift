//
//  FavoritesWireframe.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

/// Navigation Related
class FavoritesWireFrame {
    
    private let baseController: UIViewController
    private weak var flowViewController: UIViewController?
    
    init(baseController: UIViewController) {
        self.baseController = baseController
    }
    
    /**
        Starts UI for FavoritesVC
        Starts router creating the baseFlowViewController
     */
    func showFavoritesVC(with presenter: FavoritesPresenter) {
        let favVC = FavoritesVC(presenter: presenter)
        favVC.modalPresentationStyle = .fullScreen
        flowViewController = favVC
        baseController.present(favVC, animated: false, completion: nil)
    }
    
}
