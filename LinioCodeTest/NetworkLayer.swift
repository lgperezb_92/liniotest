//
//  NetworkLayer.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import ObjectMapper

protocol NetworkLayer {
    func get<T: Mappable>(from urlStr: String, completion: @escaping((Result<[T], NetworkError>)-> ()))
    func downloadData(from urlStr: String, completion: @escaping((Result<Data, NetworkError>) -> ()))
}
