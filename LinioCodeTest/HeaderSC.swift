//
//  HeaderSC.swift
//  LinioCodeTest
//
//  Created by Gael on 18/09/21.
//

import IGListKit

class HeaderSC: ListSectionController {
    
    private var items: [HeaderVM] = []
    
    override init() {
        super.init()
        minimumLineSpacing = 10
    }
    
    override func didUpdate(to object: Any) {
        guard let data = object as? IGListSection,
              let customItems = data.viewModels as? [HeaderVM] else { return }
        items = customItems
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let size = UIScreen.main.bounds.size
        return CGSize(width: size.width, height: 50)
    }
      
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let nibName = String(describing: HeaderCell.self)
        
        guard let ctx = collectionContext else {
            return UICollectionViewCell()
        }
      
        let cell = ctx.dequeueReusableCell(withNibName: nibName,
                                           bundle: nil,
                                           for: self,
                                           at: index)
        
        let model = items[index]
        guard let HeaderCell = cell as? HeaderCell else { return cell }
        HeaderCell.setup(with: model)
        
        return cell
    }
}
