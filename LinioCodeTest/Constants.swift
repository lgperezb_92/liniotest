//
//  Constants.swift
//  LinioCodeTest
//
//  Created by Gael on 18/09/21.
//

import Foundation

enum Constants {
    static var favoritesURL = "https://gist.githubusercontent.com/aletomm90/7ff8e9a7c49aefd06a154fe097028d27/raw/c87e2e7d21313391d412420b4254c391aa68eeec/favorites.json"
}
