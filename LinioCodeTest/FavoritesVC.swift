//
//  FavoritesVC.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit
import IGListKit

class FavoritesVC: UIViewController {

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .primaryBackground
        return view
    }()
    
    lazy private var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    private var presenter: FavoritesPresenter
    private var dataSource: FavoritesDataSource = FavoritesDataSource()
    override func viewDidLoad() {
        super.viewDidLoad()
        /// Get ListDiffable data
        presenter.getListModels(completion: { result in
            DispatchQueue.main.async { [weak self] in
                self?.dataSource.updateData(data: result)
                self?.adapter.reloadData(completion: nil)
            }
        })
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        let constraints = [
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
        
        adapter.collectionView = collectionView
        adapter.dataSource = dataSource 
    }

    init(presenter: FavoritesPresenter) {
        self.presenter = presenter
        super.init(nibName: "FavoritesVC", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
