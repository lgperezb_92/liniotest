//
//  ProductSnapshotVM.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import IGListKit

class ProductSnapshotVM: IGListItem {
    
    var id: Int
    var imageURL: String
    var shippingImage: UIImage?
    var linioLevelImage: UIImage?
    var conditionImage: UIImage?
    var importedImage: UIImage?
    var heartImage: UIImage?
    
    init(with product: ProductSnapshot) {
        imageURL = product.image
        id = product.id
        heartImage = #imageLiteral(resourceName: "favorite")
        
        if product.freeShipping { shippingImage = #imageLiteral(resourceName: "freeShipping") }
        if product.imported { importedImage = #imageLiteral(resourceName: "imported") }
        
        switch product.condition {
        case .new: conditionImage = #imageLiteral(resourceName: "newProduct")
        case .refurbished: conditionImage = #imageLiteral(resourceName: "refurbishedProduct")
        }
        
        switch product.linioLevel {
        case .level1: linioLevelImage = #imageLiteral(resourceName: "linioLevel1")
        case .level2: linioLevelImage = #imageLiteral(resourceName: "linioLevel2")
        }
        
    }
}
