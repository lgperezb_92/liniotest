//
//  UIColor.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

/// Ideally an App needs to have a small and well defined library for colors
extension UIColor {
    static var primaryBackground: UIColor { return #colorLiteral(red: 0.9725490196, green: 0.9607843137, blue: 0.9607843137, alpha: 1) }
    static var secondaryBackgroud: UIColor { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) }
    static var secondaryBackground2: UIColor { return #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1) }
    static var primaryText: UIColor { return #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1) }
    static var secondaryText: UIColor { return #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1) }
}
