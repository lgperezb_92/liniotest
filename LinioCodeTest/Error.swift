//
//  Error.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import Foundation

enum NetworkError: Error {
    case badDataFormat
    case serverError
    case badURL
}
