//
//  HeaderCell.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

class HeaderCell: UICollectionViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
         titleLabel.text = ""
    }
    
    func setup(with viewModel: HeaderVM) {
        titleLabel.text = viewModel.text
        titleLabel.textColor = viewModel.textColor
        titleLabel.font = viewModel.font
    }

}
