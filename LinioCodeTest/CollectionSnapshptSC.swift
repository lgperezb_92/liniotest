//
//  CollectionSnapshotSC.swift
//  LinioCodeTest
//
//  Created by Gael on 18/09/21.
//

import IGListKit

class CollectionSnapshotSC: ListSectionController {
    
    private var items: [CollectionSnapshotVM] = []
    
    override init() {
        super.init()
        minimumLineSpacing = 9
        inset = UIEdgeInsets(top: 9, left: 9, bottom: 9, right: 9)
    }
    
    override func didUpdate(to object: Any) {
        guard let data = object as? IGListSection,
              let customItems = data.viewModels as? [CollectionSnapshotVM] else { return }
        items = customItems
    }
    
    override func numberOfItems() -> Int {
        return items.count
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let size = UIScreen.main.bounds.size
        let width = size.width/2 - 15
        return CGSize(width: width, height: width)
    }
      
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let nibName = String(describing: CollectionSnapshotCell.self)
        
        guard let ctx = collectionContext else {
            return UICollectionViewCell()
        }
        let model = items[index]
        let cell = ctx.dequeueReusableCell(withNibName: nibName,
                                           bundle: nil,
                                           for: self,
                                           at: index)
        guard let HeaderCell = cell as? CollectionSnapshotCell else { return cell }
        HeaderCell.setup(with: model)
        
        return cell
    }
}
