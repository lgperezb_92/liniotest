//
//  CollectionSnapshotCell.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

class CollectionSnapshotCell: UICollectionViewCell {

    @IBOutlet private weak var firstImageView: UIImageView!
    @IBOutlet private weak var secondImageView: UIImageView!
    @IBOutlet private weak var thirdImageView: UIImageView!
    @IBOutlet private weak var imagesContainerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        firstImageView.isHidden = true
        secondImageView.isHidden = true
        thirdImageView.isHidden = true
    }
    
    func setup(with viewModel: CollectionSnapshotVM) {
        
        // Labels
        titleLabel.text = viewModel.title
        titleLabel.textColor = viewModel.titleColor
        titleLabel.font = viewModel.titleFont
        subtitleLabel.text = viewModel.subtitle
        subtitleLabel.textColor = viewModel.subtitleColor
        subtitleLabel.font = viewModel.subtitleFont
        firstImageView.isHidden = false
        
        // ImageView & Containers
        firstImageView.roundCorners(with: 8)
        secondImageView.roundCorners(with: 8)
        thirdImageView.roundCorners(with: 8)
        imagesContainerView.backgroundColor = .secondaryBackground2
        imagesContainerView.roundCorners(with: 8)
        
        // Setup images
        guard !viewModel.imageURLs.isEmpty else {
            firstImageView.image = #imageLiteral(resourceName: "emptycart")
            return
        }
        
        if let imageURL = viewModel.imageURLs[safe: 0] {
            firstImageView.isHidden = false
            firstImageView.loadImage(from: imageURL)
        }
        
        if let imageURL = viewModel.imageURLs[safe: 1] {
            secondImageView.isHidden = false
            secondImageView.loadImage(from: imageURL)
        }
        
        if let imageURL = viewModel.imageURLs[safe: 2] {
            thirdImageView.isHidden = false
            thirdImageView.loadImage(from: imageURL)
        }
        
    }
}
