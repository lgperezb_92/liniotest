//
//  UIFont.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

/// Ideally an App needs to have a small and well defined catalogue for fonts
extension UIFont {
    static var header: UIFont { return systemFont(ofSize: 22, weight: .bold) }
    static var header2: UIFont { return systemFont(ofSize: 14, weight: .bold) }
    static var title: UIFont { return systemFont(ofSize: 14) }
}
