//
//  FavoritesCollection.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import Foundation
import ObjectMapper

protocol FavoritesCollectionSnapshot {
    var id: Int { get set }
    var name: String { get set }
    var products: [ProductOverview] { get set }
}

class FavoritesCollection: Mappable, FavoritesCollectionSnapshot {
    
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var isDefault: Bool = false
    var owner: Owner?
    var createdAt: Date?
    var visibility: ListVisibility = .isPrivate
    var accessHash: String?
    var products: [ProductOverview] = []
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        isDefault <- map["isDefault"]
        owner <- map["owner"]
        createdAt <- (map["createdAt"], DateTransform())
        visibility <- (map["visibility"], EnumTransform<ListVisibility>())
        guard let rawDictionary = map.JSON["products"] as? [String: Any] else { return }
        
        let items = rawDictionary.compactMap { key, value -> ProductOverview? in
            guard let productDict = value as? [String: Any] else { return nil }
            guard let product = Product(JSON: productDict) else { return nil }
            return product
        }
    
        self.products = items
        
    }
    
}
