//
//  FavoritesDataSource.swift
//  LinioCodeTest
//
//  Created by Gael on 18/09/21.
//

import IGListKit

class FavoritesDataSource: NSObject, ListAdapterDataSource {
    
    private var data: [ListDiffable] = []
    
    override init() {
        super.init()
    }
    
    func updateData(data: [ListDiffable]) {
        self.data = data
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return data
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        
        guard let customObject = object as? IGListSection else { return  ListSectionController() }
        
        if customObject.viewModels is [HeaderVM] {
            return HeaderSC()
        } else if customObject.viewModels is [CollectionSnapshotVM] {
            return CollectionSnapshotSC()
        } else if customObject.viewModels is [ProductSnapshotVM] {
            return ProductSnapshotSC()
        }
        
        return ListSectionController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }

}
