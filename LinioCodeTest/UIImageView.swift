//
//  UIImageView.swift
//  LinioCodeTest
//
//  Created by Gael on 18/09/21.
//

import UIKit

extension UIImageView {
    /**
        Download data from a URLString and try to convert it to UIImage.
        While downloading a loader is shown in the center of the UIImageView
     
        - Parameter urlString: URL for the Image
        - Parameter shouldHide: Indicates if self should be hidden in case of error. Default [True]
     */
    func loadImage(from urlString: String, shouldHide: Bool = true) {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.backgroundColor = .blue
        activityIndicator.startAnimating()
        activityIndicator.center = self.center
        self.addSubview(activityIndicator)
        
        let api = NativeNetwork()
        api.downloadData(from: urlString, completion: { [weak self] result in
            DispatchQueue.main.async {
                activityIndicator.removeFromSuperview()
                switch result {
                case .success(let data):
                    guard let image = UIImage(data: data) else {
                        if shouldHide { self?.isHidden = true }
                        return
                    }
                    self?.image = image
                case .failure(_):
                    if shouldHide { self?.isHidden = true }
                }
            }
        })
    }
}
