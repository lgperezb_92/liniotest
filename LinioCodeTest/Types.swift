//
//  Types.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import Foundation

enum ListVisibility: String {
    case isPublic = "public"
    case isPrivate = "private"
}

enum ProductCondition: String {
    case refurbished
    case new
}

enum LinioLevel: Int {
    case level1 = 1
    case level2 = 2
}

enum SectionType: Int {
    case header = 18
    case subtitle = 16
}
