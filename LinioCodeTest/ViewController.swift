//
//  ViewController.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let module = FavoritesFlowModule(baseController: self)
        module.start()
    }

}

