//
//  NativeNetwork.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import ObjectMapper

class NativeNetwork: NetworkLayer {
    
    /**
        Get any Data from a given url, and returns either the Object<Mappable> or a CustomErrorType -> NetworkError
        - Parameter urlStr: String containing the url to retrieve data
        - Parameter completion: Result<[T],NetworkError>, in case data is valid, is JSON and represents an object defined as Mappable, an array of those objects will be retrieved, otherwise error.
     */
    func get<T: Mappable>(from urlStr: String, completion: @escaping ((Result<[T], NetworkError>) -> ())) {
        
        guard let url = URL(string: urlStr) else {
            completion(.failure(.badURL))
            return
        }
        
        let session = URLSession.shared
        let urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 1)
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            
            guard let data = data, error == nil, let response = response as? HTTPURLResponse,
                  response.statusCode == 200 else {
                completion(.failure(.serverError))
                return
            }
            
            let jsonString = String(decoding: data, as: UTF8.self)
            guard let genericObject = Mapper<T>().mapArray(JSONString: jsonString) else {
                completion(.failure(.badDataFormat))
                return
            }
            
            
            completion(.success(genericObject))
        }
        
        dataTask.resume()
    }
    
    func downloadData(from urlStr: String, completion: @escaping ((Result<Data, NetworkError>) -> ())) {
        guard let url = URL(string: urlStr) else {
            completion(.failure(.badURL))
            return
        }
        
        let session = URLSession.shared
        let urlRequest = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 1)
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            
            guard let data = data, error == nil, let response = response as? HTTPURLResponse,
                  response.statusCode == 200 else {
                completion(.failure(.serverError))
                return
            }
            
            completion(.success(data))
        }
        
        dataTask.resume()
    }

}
