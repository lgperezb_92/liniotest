//
//  HeaderVM.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import IGListKit

class HeaderVM: IGListItem {
    
    private var title: String
    private var type: SectionType
    var id: Int { return title.hash }
    
    var text: String { return title }
    var font: UIFont {
        switch type {
        case .header:
            return .header
        default:
            return .header2
        }
    }
    
    var textColor: UIColor { return .primaryText }
    
    init(title: String, sectionType: SectionType) {
        self.title = title
        self.type = sectionType
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return title as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let vm = object as? HeaderVM else { return false }
        return vm.title == self.title
    }
    
    
}
