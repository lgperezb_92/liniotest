//
//  IGListKit.swift
//  LinioCodeTest
//
//  Created by Gael on 18/09/21.
//

import IGListKit

/// Must be followed for ViewModels to be able to be render by IGListKit
protocol IGListItem {
    var id: Int { get }
}

/// Helper to gather together IGListItem that belong to the same section.
class IGListSection: ListDiffable {

    var viewModels: [IGListItem] = []
    
    init(data: [IGListItem]) {
        self.viewModels = data
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return viewModels.map { $0.id } as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let section = object as? IGListSection else { return false }
        let selfIds = viewModels.map { $0.id }
        let otherIds = section.viewModels.map { $0.id }
        return selfIds == otherIds
    }
}
