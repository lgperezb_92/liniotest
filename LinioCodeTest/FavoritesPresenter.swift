//
//  FavoritesPresenter.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import IGListKit

typealias ListModelCallBack = (([ListDiffable]) -> Void)
class FavoritesPresenter {
    
    private var interactor: FavoritesInteractor
    private var router: FavoritesWireFrame
    
    init(interactor: FavoritesInteractor, router: FavoritesWireFrame) {
        self.interactor = interactor
        self.router = router
    }
        
    /**
        Get Favorites from endpoint with help from the Interactor, creates an array compatible with IGListKit
        - Parameter completion: Returns an array of [ListDiffable] creating Sections and VM needed for CollectionView Cells.
     */
    func getListModels(completion: @escaping ListModelCallBack) {
        interactor.getFavorites(completion: { result in
            
            var sections = [ListDiffable]()
            
            var totalFav = 0
            
            var favoriteGroups = [CollectionSnapshotVM]()
            var favProducts = [ProductSnapshotVM]()
            
            _ = result.map { section in
                favoriteGroups.append(CollectionSnapshotVM(from: section))
                totalFav += section.products.count
                
            _ = section.products.map { product in
                    guard let product = product as? ProductSnapshot else { return }
                    favProducts.append( ProductSnapshotVM(with: product))
                }
            }
            
            
            let titleVM = HeaderVM(title: "Favoritos", sectionType: .header)
            let titleSection  = IGListSection(data: [titleVM])
            
            let favoriteColletionSection = IGListSection(data: favoriteGroups)
            
            let titleVM2 = HeaderVM(title: "Todos mis favoritos (\(totalFav))",
                                               sectionType: .subtitle)
            let titleSection2 = IGListSection(data: [titleVM2])
            
            let allFavoritesCollection = IGListSection(data: favProducts)

            sections.append(titleSection)
            sections.append(favoriteColletionSection)
            sections.append(titleSection2)
            sections.append(allFavoritesCollection)
            
            completion(sections)
        })
    }
    
    /// Add FavoritesVC to viewHierachy with help from Router
    func start() {
        router.showFavoritesVC(with: self)
    }
}
