//
//  Product.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import Foundation
import ObjectMapper

protocol ProductOverview {
    var id: Int { get set }
    var image: String { get set }
}

protocol ProductSnapshot: ProductOverview {
    var imported: Bool { get set }
    var linioLevel: LinioLevel { get set }
    var condition: ProductCondition { get set }
    var freeShipping: Bool { get set }
}

class Product: Mappable, ProductSnapshot, ProductOverview {
    
    var id: Int = 0
    var name: String = ""
    var wishListPrice: Int = 0
    var slug: String = ""
    var url: String = ""
    var image: String = ""
    var linioLevel: LinioLevel = .level1
    var condition: ProductCondition = .new
    var freeShipping: Bool = false
    var imported: Bool = false
    var active: Bool = false
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        id              <- map["id"]
        name            <- map["name"]
        wishListPrice   <- map["whisListPrice"]
        slug            <- map["slug"]
        url             <- map["url"]
        image           <- map["image"]
        linioLevel      <- (map["linioPlusLevel"], EnumTransform<LinioLevel>())
        condition       <- (map["conditionType"], EnumTransform<ProductCondition>())
        freeShipping    <- map["freeShipping"]
        imported        <- map["imported"]
        active          <- map["active"]
    }
}
