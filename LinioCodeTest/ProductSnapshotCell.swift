//
//  ProductSnapshotCell.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

class ProductSnapshotCell: UICollectionViewCell {

    @IBOutlet private weak var favoriteImageView: UIImageView!
    @IBOutlet private weak var productImageView: UIImageView!
    @IBOutlet private weak var deliveryImageView: UIImageView!
    @IBOutlet private weak var conditionImageView: UIImageView!
    @IBOutlet private weak var importatedImageView: UIImageView!
    @IBOutlet private weak var levelImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        favoriteImageView.image = nil
        productImageView.image = nil
        deliveryImageView.image = nil
        conditionImageView.image = nil
        importatedImageView.image = nil
        levelImageView.image = nil
        deliveryImageView.isHidden = true
        conditionImageView.isHidden = true
        importatedImageView.isHidden = true
        levelImageView.isHidden = true
    }
   
    func setup(with viewModel: ProductSnapshotVM) {
        
        favoriteImageView.image = viewModel.heartImage
    
        if let deliveryImage = viewModel.shippingImage {
            deliveryImageView.isHidden = false
            deliveryImageView.image = deliveryImage
        }
        
        if let conditionImage = viewModel.conditionImage {
            conditionImageView.isHidden = false
            conditionImageView.image = conditionImage
        }
        
        if let importedImage = viewModel.importedImage {
            importatedImageView.isHidden = false
            importatedImageView.image = importedImage
        }
        
        if let levelImage = viewModel.linioLevelImage {
            levelImageView.isHidden = false
            levelImageView.image = levelImage
        }
        
        productImageView.loadImage(from: viewModel.imageURL)
        productImageView.roundCorners(with: 8)
    }
}
