//
//  UIImageView.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import UIKit

extension UIView {
    /// Round corners of a View
    /// - Parameter radius: Radius of the corners to be applied to all of them.
    func roundCorners(with radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }
}
