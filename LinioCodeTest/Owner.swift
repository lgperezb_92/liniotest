//
//  Owner.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import Foundation
import ObjectMapper

class Owner: Mappable {
    var name: String = ""
    var email: String = ""
    var linioId: String = ""
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name    <- map["name"]
        email   <- map["email"]
        linioId <- map["linioId"]
    }
}
