//
//  CollectionSnapshotVM.swift
//  LinioCodeTest
//
//  Created by Gael on 17/09/21.
//

import IGListKit

class CollectionSnapshotVM: IGListItem {
    
    var id: Int
    private var name: String
    private var products: [ProductOverview]

    var title: String { return name }
    var titleColor: UIColor { return .primaryText }
    var titleFont: UIFont { return .title }
    
    var subtitle: String { return "\(products.count)" }
    var subtitleColor: UIColor { return .secondaryText }
    var subtitleFont: UIFont { return .title }
    
    var imageURLs: [String] {
        var urls = [String]()
        for (i, product) in products.enumerated() {
            if i == 3 { break }
            urls.append(product.image)
        }
        return urls
    }
    
    init(from section: FavoritesCollectionSnapshot) {
        self.id  = section.id
        self.name = section.name
        self.products = section.products
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let vm = object as? CollectionSnapshotVM else { return false }
        return vm.id == self.id
    }
    
    
}
